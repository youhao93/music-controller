import React, { Component } from "react";
import RoompageJoinPage from "./RoomJoinPage";
import CreateRoomPage from "./CreateRoomPage";
import Room from "./Room";

import { 
    BrowserRouter as Router, 
    Switch, 
    Route, 
    Link, 
    Redirect 
} from "react-router-dom";

export default class HomePage extends Component {
    constructor(props) {
        super(props);
    }
    
// :xxx betekent dat er een variable in terecht komt

    render() {
        return (
            <Router>
                <Switch>
                    <Route exact path="/"><p>This is the homepage</p></Route>
                    <Route path="/join" component={RoompageJoinPage} />
                    <Route path="/create" component={CreateRoomPage} />
                    <Route path="/room/:roomCode" component={Room} />
                </Switch>
            </Router>);
    }
}